import { TestBed, inject } from '@angular/core/testing';

import { MachineCommService } from './machine-comm.service';

describe('MachineCommService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MachineCommService]
    });
  });

  it('should be created', inject([MachineCommService], (service: MachineCommService) => {
    expect(service).toBeTruthy();
  }));
});
