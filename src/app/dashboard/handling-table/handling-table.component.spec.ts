import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlingTableComponent } from './handling-table.component';

describe('HandlingTableComponent', () => {
  let component: HandlingTableComponent;
  let fixture: ComponentFixture<HandlingTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlingTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
