import { TestBed, inject } from '@angular/core/testing';

import { MachineListService } from './machine-list.service';

describe('MachineListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MachineListService]
    });
  });

  it('should be created', inject([MachineListService], (service: MachineListService) => {
    expect(service).toBeTruthy();
  }));
});
