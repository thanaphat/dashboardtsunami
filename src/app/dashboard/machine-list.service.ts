import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';


@Injectable()
export class MachineListService {

  private apiUrl = 'http://10.152.18.240:3011/maclist';  
  
  constructor (private http: Http) {}
  getMachineList (machine: string): Observable<MachineListModel[]> {
    const requestdata = new RequestBodyInterface(machine);
    const params = new URLSearchParams();


    const options = new RequestOptions({
    responseType: ResponseContentType.Json,
    body: requestdata,
    params: params,
    withCredentials: false
  });
    //console.log(requestdata);
    //console.log(options);
    return this.http.post(this.apiUrl,  requestdata, options)
                    .map(this.extractData)
					.catch(this.handleError);
  }



  getMachinePendingCount (): Observable<MachinePendingCountModel[]> {
    const apiUrl = 'http://10.152.18.240:3011/macpendingcount';
    const requestdata = new RequestBodyInterface('');
    const params = new URLSearchParams();


    const options = new RequestOptions({
    responseType: ResponseContentType.Json,
    body: requestdata,
    params: params,
    withCredentials: false
  });

    return this.http.post(apiUrl,  requestdata, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }




  private extractData(res: Response) {
    //console.log(res);

    let body = res.json();
    console.log(body);
    return body || { };
  }

  private handleError (error: any) {
   let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

export class RequestBodyInterface {
    private mcserial: string;
    constructor(private _mac: string) {
        this.mcserial = _mac;
    }
}

export class MachineListModel {
    mcp: string;
    ind: string;
    line: string;
    mc_serial: string;
    begin_lot: string;
}


export class MachinePendingCountModel {
  mcp: string;
  i_sim_ind_content: string;
  cnt: number;
}
