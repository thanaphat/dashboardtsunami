import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyresultComponent } from './dailyresult.component';

describe('DailyresultComponent', () => {
  let component: DailyresultComponent;
  let fixture: ComponentFixture<DailyresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
