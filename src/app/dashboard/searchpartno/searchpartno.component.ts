import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { JobStatusDashboardModel } from '../shared/jobstatusdashboard.model';
import { HandlingDataService } from '../handling-data.service';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { DatePipe } from '@angular/common';
import { MatSort, Sort, MatTable } from '@angular/material';
import { isEmpty } from 'rxjs/operator/isEmpty';
import { isNull } from 'util';
import { HandlingCommService, HandlingCommunicationInterface } from '../handling-comm.service';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Component({
  selector: 'app-searchpartno',
  templateUrl: './searchpartno.component.html',
  styleUrls: ['./searchpartno.component.css']
})
export class SearchpartnoComponent implements OnInit {

  @Input()
  partno: string;

  @Input()
  aliveFlag: boolean;

  handlingMessage: HandlingCommunicationInterface;
  displayedColumns = ['u_plan_date', 'u_shift', 'seqno', 'u_ind_content', 'i_seiban', 'i_sch_lot_no',
                     'i_plan_remark1',
                     'i_sim_item_cd', 'i_sim_po_qty', 'i_stock_location', 
                     'prc_start', 'prc_end', 'state'];
  dataJobStatusDashboard: Array<JobStatusDashboardModel>;
  jobStatusDashboardDatabase: JobStatusDashboardDatabase;
  dataSource: JobStatusDataSource | null;
  i_seiban_list: string[];
  constructor(private _handlingDataService: HandlingDataService, private _datepipe: DatePipe,
              private _handlingComm: HandlingCommService) {
     }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('table') table: MatTable<TableElement>;

  ngOnInit() {
    this.sort.active = 'u_shift';
    this.sort.direction = 'asc';
    this._handlingComm.change.subscribe(msg => {
      {
        this.handlingMessage = msg;
        this.partno = this.handlingMessage.partno;
        this.showData();
      }
    });

  }

  checkStyle(state: string): object {
      let res: object;
     
      if (state == 'Finish') { res = {'background-color': '#66BB6A', 'color': 'white'}; }
      if (state == 'On Process') { res = {'background-color': '#FFCA28'}; }
      if (state == 'Not Process') {res = {'background-color': '#FFFFFF'}; }
      return res;
  }



  showData() {
    this.jobStatusDashboardDatabase = new JobStatusDashboardDatabase(this._handlingDataService, this._datepipe, 
          this.partno, this.aliveFlag);
    this.dataSource = new JobStatusDataSource(this.jobStatusDashboardDatabase, this.sort);

  }

  sortData(sort: Sort) {
    this.dataSource.sortAll();
    this.table.renderRows();
  }

}


export class JobStatusDataSource extends DataSource<any> {
  constructor(private _jobStatusDashboardDatabase: JobStatusDashboardDatabase, private _sort: MatSort) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<JobStatusDashboardModel[]> {
    return this._jobStatusDashboardDatabase.dataChange;
  }

  disconnect() {}

  sortAll() {
    this._jobStatusDashboardDatabase.jobStatusData = this.sortData();
  }


  sortData(): JobStatusDashboardModel[] {
    const data = this._jobStatusDashboardDatabase.jobStatusData;
    if (!this._sort.active || this._sort.direction == '') { return data; }

    return data.sort((a, b) => {
        let propertyA: number|string = '';
        let propertyB: number|string = '';

        switch(this._sort.active) {
          case 'u_shift': [propertyA, propertyB] = [a.u_shift, b.u_shift]; break;
          case 'seqno' : [propertyA, propertyB] = [a.seqno, b.seqno]; break;
          case 'jobno' : [propertyA, propertyB] = [a.jobno, b.jobno]; break;
          case 'state' : [propertyA, propertyB] = [a.state, b.state]; break;
        }

        let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
        let valueB = isNaN(+propertyB) ? propertyB : +propertyB;
        return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
      
    });
  }
}

export class JobStatusDashboardDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<JobStatusDashboardModel[]> = new BehaviorSubject<JobStatusDashboardModel[]>([]);
  errorMessage: string;
  alive: boolean;
  jobStatusData: Array<JobStatusDashboardModel>;
  _seiban_list: string[];
  constructor(private _handlingDataService: HandlingDataService, private _datepipe: DatePipe,
              private _partno: string,
              private _aliveFlag: boolean) {
    this.alive = _aliveFlag;
    this.getDataFromService();
    this._seiban_list = this.callseiban_list(this.jobStatusData);
  }

  get data(): JobStatusDashboardModel[] { return this.jobStatusData; }
  get seiban_list(): string[] { return this._seiban_list; }

  callseiban_list(jobstatus: JobStatusDashboardModel[]): string[] {
    let source: string[];
    let tmp: string;
    for (var job of jobstatus) {
      if (tmp != job.i_seiban) {
          source.push(job.i_seiban)
          tmp = job.i_seiban;
      }
    }
    return source;
  }

  getDataFromService() {

      const selectDate = this._datepipe.transform(new Date(), 'yyyyMMdd');


      this.jobStatusData = [];
   
      this._handlingDataService.getJobResultByPartnoDashboard(this._partno)
                                .subscribe(
                                  datajob => {this.jobStatusData = datajob; this.dataChange.next(this.jobStatusData); },
                                  error => this.errorMessage = <any>error
                                );
  
      IntervalObservable.create(30000)
                              .takeWhile(() => this.alive) // only fires when component is alive
                              .subscribe(() => {
                                this.jobStatusData = [];
                                this._handlingDataService.getJobResultByPartnoDashboard(this._partno)
                                .subscribe(
                                  datajob => {this.jobStatusData = datajob; this.dataChange.next(this.jobStatusData); },
                                  error => this.errorMessage = <any>error
                                );
                                });
   }
}

export interface TableElement {
  u_shift: string;
  seqno: number;
  jobno: string;
  u_ind_content: string;
  i_sim_ind_content: string;
  i_sim_item_cd: string;
  i_sim_po_qty: number;
  prc_start;
  prc_end;
  state: string;
  i_plan_remark1: string;
  i_sch_lot_no: string;
}
