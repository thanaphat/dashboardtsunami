import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HandlingListComponent } from './handling-list/handling-list.component';
import { JobstatusPendingListComponent } from './jobstatus-pending-list/jobstatus-pending-list.component';
import { SearchpartnoListComponent } from './searchpartno-list/searchpartno-list.component';
import { DailyresultListComponent } from './dailyresult-list/dailyresult-list.component';
import { MachinePerfListComponent } from './machine-perf-list/machine-perf-list.component';
// import { ForecastModule } from './forecast/forecast.module';

const appRoutes: Routes = [
    // { path: 'forecast', loadChildren: 'app/forecast/forecast-routing/forecast-routing.module#ForecastRoutingModule' },
    { path: 'dashboard/dailyplan', component: HandlingListComponent },
    { path: 'dashboard/pending', component: JobstatusPendingListComponent },
    { path: 'dashboard/searchbypartno', component: SearchpartnoListComponent},
    { path: 'dashboard/dailyresult', component: DailyresultListComponent},
    { path: 'dashboard/machineperf', component: MachinePerfListComponent},
	{ path: 'dashboard/machineperf/:mcs', component: MachinePerfListComponent},
    { path: '**', component: PageNotFoundComponent }
  ];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forRoot(appRoutes,
                          { enableTracing: true }
                  )
        ],
    exports: [ RouterModule ],
    providers: []
})

export class DashboardRoutingModule {}


