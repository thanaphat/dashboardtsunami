import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HandlingListComponent } from './handling-list/handling-list.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

import {CdkTableModule} from '@angular/cdk/table';
import { HandlingTableComponent } from './handling-table/handling-table.component';
import { HandlingAmqpService } from './handling-amqp.service';
import { HandlingDataService } from './handling-data.service';
import { TrimPipe } from '../../helper/pipe/trim.pipe';
import { FilterUniquePipe } from '../../helper/pipe/filterUnique.pipe';
import { FormsModule } from '@angular/forms';
import { MachineListService } from './machine-list.service';
import { HandlingCommService } from './handling-comm.service';
import { JobstatusPendingComponent } from './jobstatus-pending/jobstatus-pending.component';
import { JobstatusPendingListComponent } from './jobstatus-pending-list/jobstatus-pending-list.component';
import { StatusCurrentAllMachinesComponent } from './status-current-all-machines/status-current-all-machines.component';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { DashboardRoutingModule } from './dashboard.routing';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DailyresultComponent } from './dailyresult/dailyresult.component';
import { DailyresultListComponent } from './dailyresult-list/dailyresult-list.component';
import { SearchpartnoListComponent } from './searchpartno-list/searchpartno-list.component';
import { SearchpartnoComponent } from './searchpartno/searchpartno.component';
import { MachinePerfComponent } from './machine-perf/machine-perf.component';
import { MachineDataService } from './machine-data.service';
import { MachinePerfListComponent } from './machine-perf-list/machine-perf-list.component';
import { MachineCommService } from './machine-comm.service';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    HttpModule,
    FormsModule,
    CommonModule
  ],
  exports: [
    DashboardMainComponent
  ],
  providers: [HandlingDataService, MachineListService, HandlingCommService, DatePipe, MachineDataService, MachineCommService],
  declarations: [HandlingListComponent, HandlingTableComponent, TrimPipe, FilterUniquePipe, JobstatusPendingComponent,
            JobstatusPendingListComponent, StatusCurrentAllMachinesComponent, DashboardMainComponent,
          PageNotFoundComponent,
          DailyresultComponent,
          DailyresultListComponent,
          SearchpartnoListComponent,
          SearchpartnoComponent,
          MachinePerfComponent,
          MachinePerfListComponent]
})
export class DashboardModule { }
