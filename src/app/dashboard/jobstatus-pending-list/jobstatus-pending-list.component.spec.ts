import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobstatusPendingListComponent } from './jobstatus-pending-list.component';

describe('JobstatusPendingListComponent', () => {
  let component: JobstatusPendingListComponent;
  let fixture: ComponentFixture<JobstatusPendingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobstatusPendingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobstatusPendingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
