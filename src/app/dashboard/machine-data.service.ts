import { Injectable } from '@angular/core';
import { MachinePerfModel } from './shared/machineperf.model';
import * as moment from 'moment';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { JobStatusDashboardModel } from './shared/jobstatusdashboard.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MachineDataService {

  private TEST_DATE: MachinePerfModel;

  constructor(private http: Http) {
    this.TEST_DATE =  {mcd: 'P40T', mc_desc: 'Press40Ton', working: [{type: 'W', item: 'W-PEE1010-12', 
    jobno: 'JA32301', start: moment('2018-05-24T10:06:15.634Z').toDate(), finish: moment('2018-05-24T11:08:15.634Z').toDate()},
    {type: 'B', item: 'W-PEE1010-21223.3',
    jobno: 'JA32302', start: moment('2018-05-24T11:08:15.634Z').toDate(), finish: moment('2018-05-24T14:06:15.634Z').toDate()},
    {type: 'W', item: 'W-PEE1010-21213.3',
    jobno: 'JA32303', start: moment('2018-05-24T14:06:15.634Z').toDate(), finish: moment('2018-05-24T15:06:15.634Z').toDate()}]};

  }


  getJobResultDashboard (plandate: string, machine: string): Observable<JobStatusDashboardModel[]> {
    const apiUrl = 'http://10.152.18.240:3011/jobresult';
    const requestdata = new RequestBodyInterface(plandate, machine);
    const params = new URLSearchParams();
    
    const options = new RequestOptions({
    responseType: ResponseContentType.Json,
    body: requestdata,
    params: params,
    withCredentials: false
  });
    
    console.log('start:e:');
    console.log(options);
    console.log(requestdata);
    const result = this.http.post(apiUrl,  requestdata, options)
                  .delay(25000 * Math.random())
                  .map(this.extractData)
                  .catch(this.handleError);

    return result;
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private handleError (error: any) {
   let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}




class RequestBodyInterface {
  private dts: string;
  private mac: string;
  private partno: string;
  constructor(private _dts: string, private _mac: string) {
      this.dts = _dts;
      this.mac = _mac;
  }

}

