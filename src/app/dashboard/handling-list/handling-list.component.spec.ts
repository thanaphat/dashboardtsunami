import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlingListComponent } from './handling-list.component';

describe('HandlingListComponent', () => {
  let component: HandlingListComponent;
  let fixture: ComponentFixture<HandlingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
