import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusCurrentAllMachinesComponent } from './status-current-all-machines.component';

describe('StatusCurrentAllMachinesComponent', () => {
  let component: StatusCurrentAllMachinesComponent;
  let fixture: ComponentFixture<StatusCurrentAllMachinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusCurrentAllMachinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusCurrentAllMachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
