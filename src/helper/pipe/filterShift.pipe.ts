import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'filterShift'})
export class FilterShift implements PipeTransform {
    transform(items: any[], args: any[]): any {
        console.log(items);
        console.log(args[0]);
        return items.filter(item => item.i_shift == args[0]);
    }
}