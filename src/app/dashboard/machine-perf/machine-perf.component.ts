import { Component, OnInit, AfterContentInit, Input, OnDestroy  } from '@angular/core';
import * as d3 from 'd3';
import * as d3ln from 'd3-timeline';
import { MachineDataService } from '../machine-data.service';
import { MachinePerfModel, MachineWorkingTimeModel} from '../shared/machineperf.model';
import * as moment from 'moment';
import { HandlingDataService } from '../handling-data.service';
import { DatePipe } from '@angular/common';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from "rxjs/Subscription";
import { JobStatusDashboardModel } from '../shared/jobstatusdashboard.model';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { isEmpty } from 'rxjs/operator/isEmpty';
import { isNull } from 'util';
import { map } from 'rxjs/operators';
import { MachineCommService, MachineCoummunicationInterface } from '../machine-comm.service';

@Component({
  selector: 'app-machine-perf',
  templateUrl: './machine-perf.component.html',
  styleUrls: ['./machine-perf.component.css']
})
export class MachinePerfComponent implements OnInit, AfterContentInit, OnDestroy {

  @Input()
  vMachine: string;

  graph_date: Date;
  data_min: Date;
  data_max: Date;

  shift_day_start_time = '0800';
  shift_day_finish_time = '2000';
  shift_night_start_time = '2000';
  

  hourly: Date[] = [];
  
  sub: Subscription;
  timer = Observable.timer(0, 300000);
  
  machineMessage: MachineCoummunicationInterface;
  

  constructor(private machineDS: MachineDataService,  private _datepipe: DatePipe
			, private _machineComm: MachineCommService) { }

  ngOnInit() {
	let currentDate: string;
	let shift_start_time: string;

	this._machineComm.change.subscribe(msg => {
		this.machineMessage = msg;
		currentDate = this.machineMessage.productionDate;
		shift_start_time = (this.machineMessage.shift == 'Day') ? this.shift_day_start_time : this.shift_night_start_time;
		
		this.data_min = moment(currentDate + shift_start_time, 'YYYYMMDDHHmm').toDate();
		this.data_max = moment(moment(this.data_min).add(12, 'hours').toDate()).add(10, 'minutes').toDate();
	
		this.sub = this.timer.subscribe(t => {
			this.hourly.length = 0;		/* Clear old data	*/
			
			let currentMachine: string;
			let datasource: MachinePerfModel = {mcd: '', mc_desc: '', working: []};

			currentMachine = this.vMachine;
			console.log (currentMachine);
			
			d3.select('#container-' + currentMachine).selectAll("*").remove();

			let i = 0;
			for (i = 0; i <= 12; i++ ) {
			  this.hourly.push(moment(this.data_min).add( i, 'hours').toDate());
			}


			datasource.mcd = currentMachine;
			datasource.working = [];
			this.machineDS.getJobResultDashboard(currentDate, currentMachine)
									.subscribe(data => {
										data.forEach( element => {
											let workingTime: MachineWorkingTimeModel = {type: '', jobno: '',
											item: '', start: new Date(), finish: new Date()};

											workingTime.type = element.type;
											workingTime.jobno = element.jobno;
											workingTime.item = element.i_sim_item_cd;
											workingTime.start = (element.prc_start <= this.data_min) ? this.data_min : element.prc_start ;
											workingTime.finish = (element.prc_end >= this.data_max) ? this.data_max : element.prc_end;
											datasource.working.push(workingTime);
										});
										this.ShowContent(currentMachine, datasource);
									});
		});
	});
  }

  ngAfterContentInit() {

  }
  
  ngOnDestroy(){
	this.sub.unsubscribe();
	this._machineComm.change.unsubscribe();
  }
  
  ShowContent(_machine: string, datasource: MachinePerfModel) {

    const graph_width = 1500;
    const graph_height = 140;
    const margin_graphLeft = 200;
    const margin_graphRight = 50;
    const margin_graphTop = 30;
    const bar_width = 60;
    const bar_gap = 10;

    let domain_min = moment(this.data_min).valueOf();
    let domain_max = moment(this.data_max).valueOf();
    let domain_range: number;
    let data: MachineWorkingTimeModel;
    let firstOffset: number;
    let machine: string


    machine = _machine;
    domain_range = domain_min - domain_max;


    let xscale = d3.scaleLinear()
        .domain([domain_min, domain_max])
        .range([margin_graphLeft, graph_width - margin_graphLeft]);
	
	xscale.clamp(true);

    let name = '#svg-' + machine;

    let svg = d3.select('#container-' + machine)
        .append('svg')
        .attr('id', 'svg-' + machine)
        .attr('width', graph_width)
        .attr('height', graph_height)
        .attr('style', 'outline: thin solid #f5f5dc');
    
    svg = d3.select(name)
    .append('text')
    .attr('x', 20)
    .attr('y', 100)
    .attr('r', 100)
    .attr('font-family', 'Tahoma')
    .attr('font-size', '2em')
    .attr('color', 'black')
    .text(machine);

    svg = d3.select(name)
          .append('rect')
          .attr( 'x', xscale(moment(this.data_min).valueOf()))
          .attr( 'y', 20)
          .attr( 'height', 2)
          .attr( 'width', xscale(moment(this.data_max).valueOf()) - xscale(moment(this.data_min).valueOf()))
          .attr( 'fill', '#dcdcdc');

    svg = d3.select(name);
    this.hourly.map ( function (d, i) {
        svg.append('text')
           .attr( 'x', xscale(moment(d).valueOf()))
           .attr( 'y', 15)
           .attr('font-family', 'Tahoma')
           .attr('font-size', '12px')
           .attr('fill', 'gray')
           .text(moment(d).format('HH:mm'));
    });

    svg = d3.select(name)
            .append('rect')
            .attr( 'x', xscale(moment(this.data_min).valueOf()))
            .attr( 'y', margin_graphTop)
            .attr( 'height', bar_width )
            .attr( 'width', xscale(moment().valueOf()) - xscale(moment(this.data_min).valueOf()))
            .attr( 'fill', '#e6e6e6');

    svg = d3.select(name);
    datasource.working.map( function (d, i) {
      svg.append('rect')
      .attr( 'x', xscale(moment(d.start).valueOf()))
	  .attr( 'y', margin_graphTop)
      .attr( 'height', bar_width )
      .attr( 'width', xscale(moment(d.finish).valueOf()) - xscale(moment(d.start).valueOf()))
      .attr( 'fill', function () {
		  let color = 'steelblue';
		  data = d;
		  switch (data.type) {
			/*	Break	*/
			case 'B' : { color = 'rgba(255, 227, 0, 0.5)';
						 break;
					   }
			/*	Working (Finish)	*/
			case 'W' : { color = 'rgba(0, 100, 0, 0.5)';
						 break;
						}
			/*	Maintainance	*/
			case 'M' : { color = 'rgba(170, 0, 0, 0.5)';
						break;
					   }
			/*	Working (On Process)	*/
			case 'O' : { color = 'rgba(0, 0, 0, 0.5)';
						break;
					  }
			/*	No state	*/
			case 'A' : { color = 'rgba(85, 85, 85, 0.5)';
					   break;
					  }
		  }
		  return color; })
	  .attr( 'stroke', function(){
		  let color = 'steelblue';
		  data = d;
		  switch (data.type) {
			/*	Break	*/
			case 'B' : { color = 'rgba(255, 227, 0, 0.9)';
						 break;
					   }
			/*	Working (Finish)	*/
			case 'W' : { color = 'rgba(0, 100, 0, 0.9)';
						 break;
						}
			/*	Maintainance	*/
			case 'M' : { color = 'rgba(170, 0, 0, 0.9)';
						break;
					   }
			/*	Working (On Process)	*/
			case 'O' : { color = 'rgba(0, 0, 0, 0.9)';
						break;
					  }
			/*	No state	*/
			case 'A' : { color = 'rgba(85, 85, 85, 0.9)';
					   break;
					  }
		  }
		  return color;
		 });
    });

  }

}


