import { Component, OnInit } from '@angular/core';
import { HandlingTableComponent } from '../handling-table/handling-table.component';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MachineListService, MachineListModel } from '../machine-list.service';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { HandlingCommService, HandlingCommunicationInterface } from '../handling-comm.service';

export class AppDateAdapter extends NativeDateAdapter {
  parse(value: any): Date | null {
    if (typeof value === "string" && value.indexOf("/") > -1) {
      const str = value.split("/");
      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);
      return new Date(year, month, date);
    }
    const timestamp = typeof value === "number" ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      //return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
      return year + "" + this._to2digit(month) + "" + this._to2digit(day);
    } else {
      return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ("00" + n).slice(-2);
  }
}
export const APP_DATE_FORMATS = {
  parse: {
    dateInput: { month: "short", year: "numeric", day: "numeric" }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: "input",
    monthYearLabel: { month: "short", year: "numeric", day: "numeric" },
    dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
    monthYearA11yLabel: { year: "numeric", month: "long" }
  }
};




@Component({
  selector: 'app-handling-list',
  templateUrl: './handling-list.component.html',
  styleUrls: ['./handling-list.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
]
})
export class HandlingListComponent implements OnInit {
  calendarDate: string;
  productionDate: string;
  refreshToggle: boolean;
  errorMessage: string;
  selectedValue: string;
  handlingMessage: HandlingCommunicationInterface;
  machineList: MachineListModel[];
  constructor(private _machineListServcie: MachineListService, private _handlingComm: HandlingCommService) {
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
      this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1 ) + this._to2digit(event.value.getDate());
  }

  ngOnInit() {
    this.refreshToggle = true;
    this.getMachineList();

  }

  getMachineList() {
    this._machineListServcie.getMachineList('')
    .subscribe(
      machine => this.machineList = machine,
      error => this.errorMessage = <any>error
    );
  }

  updateRequest() {
    this._handlingComm.submitData(this.productionDate, this.selectedValue, this.refreshToggle)
  }

}
