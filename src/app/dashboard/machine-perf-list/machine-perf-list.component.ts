import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MachineListService, MachineListModel } from '../machine-list.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MachineCommService, MachineCoummunicationInterface } from '../machine-comm.service';


@Component({
  selector: 'app-machine-perf-list',
  templateUrl: './machine-perf-list.component.html',
  styleUrls: ['./machine-perf-list.component.css']
})
export class MachinePerfListComponent implements OnInit {

  _listMachine: string[] = [];
  hasParam: boolean = false;
  
  calendarDate: Date;
  selectedShift: string;
  productionDate: string;

  constructor(private route: ActivatedRoute, private mcListService: MachineListService
			, private machineCommService: MachineCommService) {
	let tmp_mc: string[] = [];
	
	this.route.queryParams
		.subscribe(p => {
			if(p.mcs != null){
				this.hasParam = true;
				this._listMachine = p.mcs.trim().split(",");
			}else{
				this.hasParam = false;
				this.mcListService.getMachineList('')
					.subscribe(obj => {
						obj.map(d => {
							//console.log(d.ind);
							this._listMachine.push(d.ind);
						});

					},() => {},() => { console.log("Subscribe complete") });

			}
		});
  }

  ngOnInit() {
	
  }
  
  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
	this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1 ) + this._to2digit(event.value.getDate());
  }
  
  updateRequest(){
	this.machineCommService.submitData(this.productionDate, this.selectedShift);
  }

}
