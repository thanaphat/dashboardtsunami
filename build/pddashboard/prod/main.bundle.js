webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n    <app-dashboard-main>\n    </app-dashboard-main>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard_module__ = __webpack_require__("./src/app/dashboard/dashboard.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard_module__["a" /* DashboardModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dailyresult-list/dailyresult-list.component.css":
/***/ (function(module, exports) {

module.exports = ".md-button-right refresh {\r\n    position: absolute;\r\n    right: 0px;\r\n    margin: 0;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/dailyresult-list/dailyresult-list.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-divider></mat-divider>\n<div class='mat-title'>\n    Daily Result\n</div>\n<div>\n  <mat-divider></mat-divider>\n  <mat-form-field>\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\" [(ngModel)]=\"calendarDate\" (dateInput)=\"addEvent('input', $event)\"\n      (dateChange)=\"addEvent('change', $event)\">\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n    <mat-datepicker #picker></mat-datepicker>\n  </mat-form-field>\n  <mat-form-field>\n    <mat-select placeholder=\"Choose a machine\" [(ngModel)]=\"selectedValue\" >\n      <mat-option *ngFor=\"let mc of machineList\" [value]=\"mc.ind\">\n        {{ mc.mcp }} ({{ mc.ind}})\n      </mat-option>\n    </mat-select>\n  </mat-form-field>\n\n  <button (click)=\"updateRequest()\" mat-raised-button>View Result</button>\n\n</div>\n<div>\n  <mat-slide-toggle  color=\"primary\" [(ngModel)]=\"refreshToggle\">Auto Refresh!</mat-slide-toggle>\n</div>\n<mat-divider></mat-divider>\n<div>\n  <mat-tab-group>\n    <mat-tab label=\"Daily Result\">\n      <app-dailyresult>\n      </app-dailyresult>\n    </mat-tab>\n  </mat-tab-group>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dailyresult-list/dailyresult-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export AppDateAdapter */
/* unused harmony export APP_DATE_FORMATS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyresultListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__machine_list_service__ = __webpack_require__("./src/app/dashboard/machine-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppDateAdapter = /** @class */ (function (_super) {
    __extends(AppDateAdapter, _super);
    function AppDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppDateAdapter.prototype.parse = function (value) {
        if (typeof value === "string" && value.indexOf("/") > -1) {
            var str = value.split("/");
            var year = Number(str[2]);
            var month = Number(str[1]) - 1;
            var date = Number(str[0]);
            return new Date(year, month, date);
        }
        var timestamp = typeof value === "number" ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    };
    AppDateAdapter.prototype.format = function (date, displayFormat) {
        if (displayFormat == "input") {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            //return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
            return year + "" + this._to2digit(month) + "" + this._to2digit(day);
        }
        else {
            return date.toDateString();
        }
    };
    AppDateAdapter.prototype._to2digit = function (n) {
        return ("00" + n).slice(-2);
    };
    return AppDateAdapter;
}(__WEBPACK_IMPORTED_MODULE_2__angular_material__["K" /* NativeDateAdapter */]));

var APP_DATE_FORMATS = {
    parse: {
        dateInput: { month: "short", year: "numeric", day: "numeric" }
    },
    display: {
        // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
        dateInput: "input",
        monthYearLabel: { month: "short", year: "numeric", day: "numeric" },
        dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
        monthYearA11yLabel: { year: "numeric", month: "long" }
    }
};
var DailyresultListComponent = /** @class */ (function () {
    function DailyresultListComponent(_machineListServcie, _handlingComm) {
        this._machineListServcie = _machineListServcie;
        this._handlingComm = _handlingComm;
    }
    DailyresultListComponent.prototype._to2digit = function (n) {
        return ('00' + n).slice(-2);
    };
    DailyresultListComponent.prototype.addEvent = function (type, event) {
        this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1) + this._to2digit(event.value.getDate());
    };
    DailyresultListComponent.prototype.ngOnInit = function () {
        this.refreshToggle = true;
        this.getMachineList();
    };
    DailyresultListComponent.prototype.getMachineList = function () {
        var _this = this;
        this._machineListServcie.getMachineList('')
            .subscribe(function (machine) { return _this.machineList = machine; }, function (error) { return _this.errorMessage = error; });
    };
    DailyresultListComponent.prototype.updateRequest = function () {
        this._handlingComm.submitData(this.productionDate, this.selectedValue, this.refreshToggle);
    };
    DailyresultListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dailyresult-list',
            template: __webpack_require__("./src/app/dashboard/dailyresult-list/dailyresult-list.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/dailyresult-list/dailyresult-list.component.css")],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["a" /* DateAdapter */], useClass: AppDateAdapter
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MAT_DATE_FORMATS */], useValue: APP_DATE_FORMATS
                }
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__machine_list_service__["a" /* MachineListService */], __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__["a" /* HandlingCommService */]])
    ], DailyresultListComponent);
    return DailyresultListComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dailyresult/dailyresult.component.css":
/***/ (function(module, exports) {

module.exports = "/* Structure */\r\n.handling-table-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n.mat-table {\r\n    display: table;\r\n    width: 100%;\r\n    overflow: auto;\r\n    }\r\n.cdk-header-row {\r\n      position: sticky;\r\n      position: -webkit-sticky;\r\n      top: 0;\r\n      background-color: inherit;\r\n  }\r\n/*\r\n   * Styles to make the demo's cdk-table match the material design spec\r\n   * https://material.io/guidelines/components/data-tables.html\r\n   */\r\n.handling-table-table {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n.handling-table-header-row {\r\n    background: #3E50B4 ;\r\n    color: white;\r\n  }\r\n.handling-table-header-row, .handling-table-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border-bottom: 1px solid #ccc;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 32px;\r\n    padding: 0 8px;\r\n  }\r\n.handling-table-cell, .handling-table-header-cell {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1;\r\n            flex: 1;\r\n  }\r\n.handling-table-header-cell {\r\n    font-size: 16px;     \r\n  }\r\n.cdk-cell:nth-child(1),\r\n  .cdk-header-cell:nth-child(1)\r\n   {\r\n   -webkit-box-flex: 0;\r\n       -ms-flex: 0 0 7%;\r\n           flex: 0 0 7%;\r\n   }\r\n.cdk-cell:nth-child(2),\r\n   .cdk-header-cell:nth-child(2)\r\n    {\r\n    -webkit-box-flex: 0;\r\n        -ms-flex: 0 0 4%;\r\n            flex: 0 0 4%;\r\n    }\r\n.cdk-cell:nth-child(3),\r\n    .cdk-header-cell:nth-child(3)\r\n     {\r\n     -webkit-box-flex: 0;\r\n         -ms-flex: 0 0 4%;\r\n             flex: 0 0 4%;\r\n     }\r\n.cdk-cell:nth-child(4),\r\n    .cdk-header-cell:nth-child(4)\r\n     {\r\n     -webkit-box-flex: 0;\r\n         -ms-flex: 0 0 10%;\r\n             flex: 0 0 10%;\r\n     }\r\n.cdk-cell:nth-child(7),\r\n     .cdk-header-cell:nth-child(7)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 17%;\r\n              flex: 0 0 17%;\r\n      }\r\n.cdk-cell:nth-child(8),\r\n      .cdk-header-cell:nth-child(8)\r\n       {\r\n       -webkit-box-flex: 0;\r\n           -ms-flex: 0 0 17%;\r\n               flex: 0 0 17%;\r\n       }\r\n.handling-table-cell {\r\n    font-size: 13px;\r\n    color: rgba(0, 0, 0, 0.87);\r\n  }\r\n.chip-item {\r\n      width: 100%;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/dailyresult/dailyresult.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"handling-table-container\">\n    <cdk-table #table  [dataSource]=\"dataSource\" matSortActive=\"u_shift\" matSortDirection='asc' (matSortChange)=\"sortData()\" class=\"handling-table-table\" matSort>\n        <ng-container cdkColumnDef=\"u_plan_date\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_plan_date\">Plan Date</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_plan_date | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"u_shift\" >\n                    <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_shift\">Shift</cdk-header-cell>\n                    <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_shift | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"seqno\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" width=\"shrink-to-fit\" mat-sort-header=\"seqno\">Seq</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.seqno}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"jobno\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"jobno\">Job No</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.jobno | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_seiban\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"i_seiban\">Seiban</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_seiban | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sch_lot_no\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Cust.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sch_lot_no | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_plan_remark1\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Part No.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_plan_remark1 | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_item_cd\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Item Cd</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_item_cd | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_po_qty\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Plan Qty</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_po_qty | number}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_start\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Start</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_start | date:\"HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_end\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Finish</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_end | date:\"HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"state\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"state\">State</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"><mat-chip-list><mat-chip [ngStyle]=\"checkStyle(row.state)\" style='chip-item'>{{row.state}}</mat-chip></mat-chip-list></cdk-cell>\n        </ng-container>\n\n        <cdk-header-row *cdkHeaderRowDef=\"displayedColumns\" class=\"handling-table-header-row\"></cdk-header-row>\n        <cdk-row *cdkRowDef=\"let row; columns: displayedColumns\" class=\"handling-table-row\"></cdk-row>\n\n    </cdk-table>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/dailyresult/dailyresult.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyresultComponent; });
/* unused harmony export JobStatusDataSource */
/* unused harmony export JobStatusDashboardDatabase */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_data_service__ = __webpack_require__("./src/app/dashboard/handling-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DailyresultComponent = /** @class */ (function () {
    function DailyresultComponent(_handlingDataService, _datepipe, _handlingComm) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._handlingComm = _handlingComm;
        this.displayedColumns = ['u_plan_date', 'u_shift', 'seqno', 'jobno', 'i_seiban', 'i_sch_lot_no',
            'i_plan_remark1',
            'i_sim_item_cd', 'i_sim_po_qty', 'prc_start', 'prc_end', 'state'];
    }
    DailyresultComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.active = 'u_shift';
        this.sort.direction = 'asc';
        this._handlingComm.change.subscribe(function (msg) {
            {
                _this.handlingMessage = msg;
                _this.selectedProductionDate = _this.handlingMessage.productionDate;
                _this.selectedMachine = _this.handlingMessage.machine;
                _this.aliveFlag = _this.handlingMessage.alive;
                _this.showData();
            }
        });
    };
    DailyresultComponent.prototype.checkStyle = function (state) {
        var res;
        if (state == 'Finish') {
            res = { 'background-color': '#66BB6A', 'color': 'white' };
        }
        if (state == 'On Process') {
            res = { 'background-color': '#FFCA28' };
        }
        if (state == 'Not Process') {
            res = { 'background-color': '#FFFFFF' };
        }
        return res;
    };
    DailyresultComponent.prototype.showData = function () {
        this.jobStatusDashboardDatabase = new JobStatusDashboardDatabase(this._handlingDataService, this._datepipe, this.selectedProductionDate, this.selectedMachine, this.aliveFlag);
        this.dataSource = new JobStatusDataSource(this.jobStatusDashboardDatabase, this.sort);
    };
    DailyresultComponent.prototype.sortData = function (sort) {
        this.dataSource.sortAll();
        this.table.renderRows();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], DailyresultComponent.prototype, "selectedProductionDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], DailyresultComponent.prototype, "selectedMachine", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], DailyresultComponent.prototype, "aliveFlag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */])
    ], DailyresultComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('table'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["F" /* MatTable */])
    ], DailyresultComponent.prototype, "table", void 0);
    DailyresultComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dailyresult',
            template: __webpack_require__("./src/app/dashboard/dailyresult/dailyresult.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/dailyresult/dailyresult.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__handling_data_service__["a" /* HandlingDataService */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__["a" /* HandlingCommService */]])
    ], DailyresultComponent);
    return DailyresultComponent;
}());

var JobStatusDataSource = /** @class */ (function (_super) {
    __extends(JobStatusDataSource, _super);
    function JobStatusDataSource(_jobStatusDashboardDatabase, _sort) {
        var _this = _super.call(this) || this;
        _this._jobStatusDashboardDatabase = _jobStatusDashboardDatabase;
        _this._sort = _sort;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    JobStatusDataSource.prototype.connect = function () {
        return this._jobStatusDashboardDatabase.dataChange;
    };
    JobStatusDataSource.prototype.disconnect = function () { };
    JobStatusDataSource.prototype.sortAll = function () {
        this._jobStatusDashboardDatabase.jobStatusData = this.sortData();
    };
    JobStatusDataSource.prototype.sortData = function () {
        var _this = this;
        var data = this._jobStatusDashboardDatabase.jobStatusData;
        if (!this._sort.active || this._sort.direction == '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'u_shift':
                    _a = [a.u_shift, b.u_shift], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'seqno':
                    _b = [a.seqno, b.seqno], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'jobno':
                    _c = [a.jobno, b.jobno], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'state':
                    _d = [a.state, b.state], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'u_plan_date':
                    _e = [a.u_plan_date, b.u_plan_date], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'i_seiban':
                    _f = [a.i_seiban, b.i_seiban], propertyA = _f[0], propertyB = _f[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction == 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f;
        });
    };
    return JobStatusDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));

var JobStatusDashboardDatabase = /** @class */ (function () {
    function JobStatusDashboardDatabase(_handlingDataService, _datepipe, _selectedProductionDate, _selectedMachine, _aliveFlag) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._selectedProductionDate = _selectedProductionDate;
        this._selectedMachine = _selectedMachine;
        this._aliveFlag = _aliveFlag;
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.alive = _aliveFlag;
        this.getDataFromService();
    }
    Object.defineProperty(JobStatusDashboardDatabase.prototype, "data", {
        get: function () { return this.jobStatusData; },
        enumerable: true,
        configurable: true
    });
    JobStatusDashboardDatabase.prototype.getDataFromService = function () {
        var _this = this;
        var selectDate = this._datepipe.transform(new Date(), 'yyyyMMdd');
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedProductionDate) || (this._selectedProductionDate == '')) {
            this._selectedProductionDate = selectDate;
        }
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedMachine) || (this._selectedMachine == '')) {
            this._selectedMachine = 'SP';
        }
        this.jobStatusData = [];
        this._handlingDataService.getJobResultDashboard(this._selectedProductionDate, this._selectedMachine)
            .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(30000)
            .takeWhile(function () { return _this.alive; }) // only fires when component is alive
            .subscribe(function () {
            _this.jobStatusData = [];
            _this._handlingDataService.getJobResultDashboard(_this._selectedProductionDate, _this._selectedMachine)
                .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        });
    };
    return JobStatusDashboardDatabase;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard-main/dashboard-main.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/dashboard-main/dashboard-main.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n        <mat-menu #appMenu=\"matMenu\">\n                <button mat-menu-item>Settings</button>\n                <button mat-menu-item>Help</button>\n        </mat-menu>\n              \n        <button mat-icon-button [matMenuTriggerFor]=\"appMenu\">\n            <mat-icon>menu</mat-icon>\n        </button><img src='assets/img/nittologo.png' style=\"transform:scale(0.70); transform-origin: left bottom 0;\" />\n                    <span class='mat-title'>NITTO PRODUCTION (version 1.0)</span>\n</div>\n<router-outlet>\n</router-outlet>"

/***/ }),

/***/ "./src/app/dashboard/dashboard-main/dashboard-main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardMainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardMainComponent = /** @class */ (function () {
    function DashboardMainComponent() {
    }
    DashboardMainComponent.prototype.ngOnInit = function () {
    };
    DashboardMainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard-main',
            template: __webpack_require__("./src/app/dashboard/dashboard-main/dashboard-main.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/dashboard-main/dashboard-main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardMainComponent);
    return DashboardMainComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_list_handling_list_component__ = __webpack_require__("./src/app/dashboard/handling-list/handling-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_cdk_table__ = __webpack_require__("./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__handling_table_handling_table_component__ = __webpack_require__("./src/app/dashboard/handling-table/handling-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__handling_data_service__ = __webpack_require__("./src/app/dashboard/handling-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__helper_pipe_trim_pipe__ = __webpack_require__("./src/helper/pipe/trim.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__helper_pipe_filterUnique_pipe__ = __webpack_require__("./src/helper/pipe/filterUnique.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__machine_list_service__ = __webpack_require__("./src/app/dashboard/machine-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__jobstatus_pending_jobstatus_pending_component__ = __webpack_require__("./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__jobstatus_pending_list_jobstatus_pending_list_component__ = __webpack_require__("./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__status_current_all_machines_status_current_all_machines_component__ = __webpack_require__("./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__dashboard_main_dashboard_main_component__ = __webpack_require__("./src/app/dashboard/dashboard-main/dashboard-main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__dashboard_routing__ = __webpack_require__("./src/app/dashboard/dashboard.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__page_not_found_page_not_found_component__ = __webpack_require__("./src/app/dashboard/page-not-found/page-not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__dailyresult_dailyresult_component__ = __webpack_require__("./src/app/dashboard/dailyresult/dailyresult.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__dailyresult_list_dailyresult_list_component__ = __webpack_require__("./src/app/dashboard/dailyresult-list/dailyresult-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__searchpartno_list_searchpartno_list_component__ = __webpack_require__("./src/app/dashboard/searchpartno-list/searchpartno-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__searchpartno_searchpartno_component__ = __webpack_require__("./src/app/dashboard/searchpartno/searchpartno.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_17__dashboard_routing__["a" /* DashboardRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["c" /* MatAutocompleteModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["d" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["e" /* MatButtonToggleModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["f" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["g" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["h" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["E" /* MatStepperModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["i" /* MatDatepickerModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["j" /* MatDialogModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["k" /* MatDividerModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["l" /* MatExpansionModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["m" /* MatGridListModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["n" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["o" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["p" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["q" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["r" /* MatNativeDateModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["s" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["t" /* MatProgressBarModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["u" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["v" /* MatRadioModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["w" /* MatRippleModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["x" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["y" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["A" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["z" /* MatSlideToggleModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["B" /* MatSnackBarModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["D" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["G" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["H" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["I" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["J" /* MatTooltipModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_16__dashboard_main_dashboard_main_component__["a" /* DashboardMainComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_7__handling_data_service__["a" /* HandlingDataService */], __WEBPACK_IMPORTED_MODULE_11__machine_list_service__["a" /* MachineListService */], __WEBPACK_IMPORTED_MODULE_12__handling_comm_service__["a" /* HandlingCommService */], __WEBPACK_IMPORTED_MODULE_1__angular_common__["d" /* DatePipe */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__handling_list_handling_list_component__["a" /* HandlingListComponent */], __WEBPACK_IMPORTED_MODULE_6__handling_table_handling_table_component__["a" /* HandlingTableComponent */], __WEBPACK_IMPORTED_MODULE_8__helper_pipe_trim_pipe__["a" /* TrimPipe */], __WEBPACK_IMPORTED_MODULE_9__helper_pipe_filterUnique_pipe__["a" /* FilterUniquePipe */], __WEBPACK_IMPORTED_MODULE_13__jobstatus_pending_jobstatus_pending_component__["a" /* JobstatusPendingComponent */],
                __WEBPACK_IMPORTED_MODULE_14__jobstatus_pending_list_jobstatus_pending_list_component__["a" /* JobstatusPendingListComponent */], __WEBPACK_IMPORTED_MODULE_15__status_current_all_machines_status_current_all_machines_component__["a" /* StatusCurrentAllMachinesComponent */], __WEBPACK_IMPORTED_MODULE_16__dashboard_main_dashboard_main_component__["a" /* DashboardMainComponent */],
                __WEBPACK_IMPORTED_MODULE_18__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_19__dailyresult_dailyresult_component__["a" /* DailyresultComponent */],
                __WEBPACK_IMPORTED_MODULE_20__dailyresult_list_dailyresult_list_component__["a" /* DailyresultListComponent */],
                __WEBPACK_IMPORTED_MODULE_21__searchpartno_list_searchpartno_list_component__["a" /* SearchpartnoListComponent */],
                __WEBPACK_IMPORTED_MODULE_22__searchpartno_searchpartno_component__["a" /* SearchpartnoComponent */]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__page_not_found_page_not_found_component__ = __webpack_require__("./src/app/dashboard/page-not-found/page-not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_list_handling_list_component__ = __webpack_require__("./src/app/dashboard/handling-list/handling-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__jobstatus_pending_list_jobstatus_pending_list_component__ = __webpack_require__("./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__searchpartno_list_searchpartno_list_component__ = __webpack_require__("./src/app/dashboard/searchpartno-list/searchpartno-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__dailyresult_list_dailyresult_list_component__ = __webpack_require__("./src/app/dashboard/dailyresult-list/dailyresult-list.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







// import { ForecastModule } from './forecast/forecast.module';
var appRoutes = [
    // { path: 'forecast', loadChildren: 'app/forecast/forecast-routing/forecast-routing.module#ForecastRoutingModule' },
    { path: 'dashboard/dailyplan', component: __WEBPACK_IMPORTED_MODULE_3__handling_list_handling_list_component__["a" /* HandlingListComponent */] },
    { path: 'dashboard/pending', component: __WEBPACK_IMPORTED_MODULE_4__jobstatus_pending_list_jobstatus_pending_list_component__["a" /* JobstatusPendingListComponent */] },
    { path: 'dashboard/searchbypartno', component: __WEBPACK_IMPORTED_MODULE_5__searchpartno_list_searchpartno_list_component__["a" /* SearchpartnoListComponent */] },
    { path: 'dashboard/dailyresult', component: __WEBPACK_IMPORTED_MODULE_6__dailyresult_list_dailyresult_list_component__["a" /* DailyresultListComponent */] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_2__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */] }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            declarations: [],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(appRoutes, { enableTracing: true })
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]],
            providers: []
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/dashboard/handling-comm.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandlingCommService; });
/* unused harmony export HandlingCommunicationInterface */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HandlingCommService = /** @class */ (function () {
    function HandlingCommService() {
        this._handlingComm = {
            productionDate: '',
            machine: '',
            partno: '',
            alive: false
        };
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */]();
    }
    HandlingCommService.prototype.submitData = function (_productionDate, _machine, _alive) {
        this._handlingComm.productionDate = _productionDate;
        this._handlingComm.machine = _machine;
        this._handlingComm.alive = _alive;
        this.change.emit(this._handlingComm);
    };
    HandlingCommService.prototype.submitPartno = function (_partno) {
        this._handlingComm.partno = _partno;
        this.change.emit(this._handlingComm);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */])
    ], HandlingCommService.prototype, "change", void 0);
    HandlingCommService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], HandlingCommService);
    return HandlingCommService;
}());

var HandlingCommunicationInterface = /** @class */ (function () {
    function HandlingCommunicationInterface() {
    }
    return HandlingCommunicationInterface;
}());



/***/ }),

/***/ "./src/app/dashboard/handling-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandlingDataService; });
/* unused harmony export RequestBodyInterface */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HandlingDataService = /** @class */ (function () {
    function HandlingDataService(http) {
        this.http = http;
    }
    HandlingDataService.prototype.getJobStatusDashboard = function (plandate, machine) {
        var apiUrl = 'http://10.152.18.240:3011/jobstatus';
        var requestdata = new RequestBodyInterface(plandate, machine);
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        return this.http.post(apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    HandlingDataService.prototype.getJobResultDashboard = function (plandate, machine) {
        var apiUrl = 'http://10.152.18.240:3011/jobresult';
        var requestdata = new RequestBodyInterface(plandate, machine);
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        return this.http.post(apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    HandlingDataService.prototype.getJobResultByPartnoDashboard = function (partno) {
        var apiUrl = 'http://10.152.18.240:3011/searchresultbypartno';
        var requestdata = new RequestBodyInterface('', '');
        requestdata.setPartno(partno);
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        return this.http.post(apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    HandlingDataService.prototype.getJobPendingDashboard = function (machine) {
        var apiUrl = 'http://10.152.18.240:3011/jobpending';
        var requestdata = new RequestBodyInterface('', machine);
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        return this.http.post(apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    HandlingDataService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    HandlingDataService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(errMsg);
    };
    HandlingDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], HandlingDataService);
    return HandlingDataService;
}());

var RequestBodyInterface = /** @class */ (function () {
    function RequestBodyInterface(_dts, _mac) {
        this._dts = _dts;
        this._mac = _mac;
        this.dts = _dts;
        this.mac = _mac;
    }
    RequestBodyInterface.prototype.setPartno = function (partno) {
        this.partno = partno;
    };
    return RequestBodyInterface;
}());



/***/ }),

/***/ "./src/app/dashboard/handling-list/handling-list.component.css":
/***/ (function(module, exports) {

module.exports = ".md-button-right refresh {\r\n    position: absolute;\r\n    right: 0px;\r\n    margin: 0;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/handling-list/handling-list.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-divider></mat-divider>\n<div class='mat-title'>\n    Daily Plan and Result\n</div>\n<div>\n  <mat-divider></mat-divider>\n  <mat-form-field>\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\" [(ngModel)]=\"calendarDate\" (dateInput)=\"addEvent('input', $event)\"\n      (dateChange)=\"addEvent('change', $event)\">\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n    <mat-datepicker #picker></mat-datepicker>\n  </mat-form-field>\n  <mat-form-field>\n    <mat-select placeholder=\"Choose a machine\" [(ngModel)]=\"selectedValue\" >\n      <mat-option *ngFor=\"let mc of machineList\" [value]=\"mc.ind\">\n        {{ mc.mcp }} ({{ mc.ind}})\n      </mat-option>\n    </mat-select>\n  </mat-form-field>\n\n  <button (click)=\"updateRequest()\" mat-raised-button>View Result</button>\n\n</div>\n<div>\n  <mat-slide-toggle  color=\"primary\" [(ngModel)]=\"refreshToggle\">Auto Refresh!</mat-slide-toggle>\n</div>\n<mat-divider></mat-divider>\n<div>\n  <mat-tab-group>\n    <mat-tab label=\"Daily Plan\">\n      <app-handling-table>\n      </app-handling-table>\n    </mat-tab>\n  </mat-tab-group>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/handling-list/handling-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export AppDateAdapter */
/* unused harmony export APP_DATE_FORMATS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandlingListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__machine_list_service__ = __webpack_require__("./src/app/dashboard/machine-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppDateAdapter = /** @class */ (function (_super) {
    __extends(AppDateAdapter, _super);
    function AppDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppDateAdapter.prototype.parse = function (value) {
        if (typeof value === "string" && value.indexOf("/") > -1) {
            var str = value.split("/");
            var year = Number(str[2]);
            var month = Number(str[1]) - 1;
            var date = Number(str[0]);
            return new Date(year, month, date);
        }
        var timestamp = typeof value === "number" ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    };
    AppDateAdapter.prototype.format = function (date, displayFormat) {
        if (displayFormat == "input") {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            //return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
            return year + "" + this._to2digit(month) + "" + this._to2digit(day);
        }
        else {
            return date.toDateString();
        }
    };
    AppDateAdapter.prototype._to2digit = function (n) {
        return ("00" + n).slice(-2);
    };
    return AppDateAdapter;
}(__WEBPACK_IMPORTED_MODULE_2__angular_material__["K" /* NativeDateAdapter */]));

var APP_DATE_FORMATS = {
    parse: {
        dateInput: { month: "short", year: "numeric", day: "numeric" }
    },
    display: {
        // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
        dateInput: "input",
        monthYearLabel: { month: "short", year: "numeric", day: "numeric" },
        dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
        monthYearA11yLabel: { year: "numeric", month: "long" }
    }
};
var HandlingListComponent = /** @class */ (function () {
    function HandlingListComponent(_machineListServcie, _handlingComm) {
        this._machineListServcie = _machineListServcie;
        this._handlingComm = _handlingComm;
    }
    HandlingListComponent.prototype._to2digit = function (n) {
        return ('00' + n).slice(-2);
    };
    HandlingListComponent.prototype.addEvent = function (type, event) {
        this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1) + this._to2digit(event.value.getDate());
    };
    HandlingListComponent.prototype.ngOnInit = function () {
        this.refreshToggle = true;
        this.getMachineList();
    };
    HandlingListComponent.prototype.getMachineList = function () {
        var _this = this;
        this._machineListServcie.getMachineList('')
            .subscribe(function (machine) { return _this.machineList = machine; }, function (error) { return _this.errorMessage = error; });
    };
    HandlingListComponent.prototype.updateRequest = function () {
        this._handlingComm.submitData(this.productionDate, this.selectedValue, this.refreshToggle);
    };
    HandlingListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-handling-list',
            template: __webpack_require__("./src/app/dashboard/handling-list/handling-list.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/handling-list/handling-list.component.css")],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["a" /* DateAdapter */], useClass: AppDateAdapter
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MAT_DATE_FORMATS */], useValue: APP_DATE_FORMATS
                }
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__machine_list_service__["a" /* MachineListService */], __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__["a" /* HandlingCommService */]])
    ], HandlingListComponent);
    return HandlingListComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/handling-table/handling-table.component.css":
/***/ (function(module, exports) {

module.exports = "/* Structure */\r\n.handling-table-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n.mat-table {\r\n    display: table;\r\n    width: 100%;\r\n    overflow: auto;\r\n    }\r\n.cdk-header-row {\r\n      position: sticky;\r\n      position: -webkit-sticky;\r\n      top: 0;\r\n      background-color: inherit;\r\n  }\r\n/*\r\n   * Styles to make the demo's cdk-table match the material design spec\r\n   * https://material.io/guidelines/components/data-tables.html\r\n   */\r\n.handling-table-table {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n.handling-table-header-row {\r\n    background: #3E50B4 ;\r\n    color: white;\r\n  }\r\n.handling-table-header-row, .handling-table-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border-bottom: 1px solid #ccc;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 32px;\r\n    padding: 0 8px;\r\n  }\r\n.handling-table-cell, .handling-table-header-cell {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1;\r\n            flex: 1;\r\n  }\r\n.handling-table-header-cell {\r\n    font-size: 16px;     \r\n  }\r\n.cdk-cell:nth-child(1),\r\n  .cdk-header-cell:nth-child(1)\r\n   {\r\n   -webkit-box-flex: 0;\r\n       -ms-flex: 0 0 4%;\r\n           flex: 0 0 4%;\r\n   }\r\n.cdk-cell:nth-child(2),\r\n   .cdk-header-cell:nth-child(2)\r\n    {\r\n    -webkit-box-flex: 0;\r\n        -ms-flex: 0 0 5%;\r\n            flex: 0 0 5%;\r\n    }\r\n.cdk-cell:nth-child(3),\r\n    .cdk-header-cell:nth-child(3)\r\n     {\r\n     -webkit-box-flex: 0;\r\n         -ms-flex: 0 0 10%;\r\n             flex: 0 0 10%;\r\n     }\r\n.cdk-cell:nth-child(5),\r\n     .cdk-header-cell:nth-child(5)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 20%;\r\n              flex: 0 0 20%;\r\n      }\r\n.cdk-cell:nth-child(6),\r\n      .cdk-header-cell:nth-child(6)\r\n       {\r\n       -webkit-box-flex: 0;\r\n           -ms-flex: 0 0 20%;\r\n               flex: 0 0 20%;\r\n       }\r\n.handling-table-cell {\r\n    font-size: 13px;\r\n    color: rgba(0, 0, 0, 0.87);\r\n  }\r\n.chip-item {\r\n      width: 100%;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/handling-table/handling-table.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"handling-table-container\">\n    <cdk-table #table  [dataSource]=\"dataSource\" matSortActive=\"u_shift\" matSortDirection='asc' (matSortChange)=\"sortData()\" class=\"handling-table-table\" matSort>\n        <ng-container cdkColumnDef=\"u_shift\" >\n                    <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_shift\">Shift</cdk-header-cell>\n                    <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_shift | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"seqno\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" width=\"shrink-to-fit\" mat-sort-header=\"seqno\">Seq</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.seqno}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"jobno\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"jobno\">Job No</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.jobno | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sch_lot_no\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Cust.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sch_lot_no | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_plan_remark1\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Part No.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_plan_remark1 | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_item_cd\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Item Cd</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_item_cd | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_po_qty\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Plan Qty</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_po_qty | number}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_start\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Start</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_start | date:\"MM/dd HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_end\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Finish</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_end | date:\"MM/dd HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"state\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"state\">State</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"><mat-chip-list><mat-chip [ngStyle]=\"checkStyle(row.state)\" style='chip-item'>{{row.state}}</mat-chip></mat-chip-list></cdk-cell>\n        </ng-container>\n\n        <cdk-header-row *cdkHeaderRowDef=\"displayedColumns\" class=\"handling-table-header-row\"></cdk-header-row>\n        <cdk-row *cdkRowDef=\"let row; columns: displayedColumns\" class=\"handling-table-row\"></cdk-row>\n\n    </cdk-table>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/handling-table/handling-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HandlingTableComponent; });
/* unused harmony export JobStatusDataSource */
/* unused harmony export JobStatusDashboardDatabase */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_data_service__ = __webpack_require__("./src/app/dashboard/handling-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HandlingTableComponent = /** @class */ (function () {
    function HandlingTableComponent(_handlingDataService, _datepipe, _handlingComm) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._handlingComm = _handlingComm;
        this.displayedColumns = ['u_shift', 'seqno', 'jobno', 'i_sch_lot_no',
            'i_plan_remark1',
            'i_sim_item_cd', 'i_sim_po_qty', 'prc_start', 'prc_end', 'state'];
    }
    HandlingTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.active = 'u_shift';
        this.sort.direction = 'asc';
        this._handlingComm.change.subscribe(function (msg) {
            {
                _this.handlingMessage = msg;
                _this.selectedProductionDate = _this.handlingMessage.productionDate;
                _this.selectedMachine = _this.handlingMessage.machine;
                _this.aliveFlag = _this.handlingMessage.alive;
                _this.showData();
            }
        });
    };
    HandlingTableComponent.prototype.checkStyle = function (state) {
        var res;
        if (state == 'Finish') {
            res = { 'background-color': '#66BB6A', 'color': 'white' };
        }
        if (state == 'On Process') {
            res = { 'background-color': '#FFCA28' };
        }
        if (state == 'Not Process') {
            res = { 'background-color': '#FFFFFF' };
        }
        return res;
    };
    HandlingTableComponent.prototype.showData = function () {
        this.jobStatusDashboardDatabase = new JobStatusDashboardDatabase(this._handlingDataService, this._datepipe, this.selectedProductionDate, this.selectedMachine, this.aliveFlag);
        this.dataSource = new JobStatusDataSource(this.jobStatusDashboardDatabase, this.sort);
    };
    HandlingTableComponent.prototype.sortData = function (sort) {
        this.dataSource.sortAll();
        this.table.renderRows();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], HandlingTableComponent.prototype, "selectedProductionDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], HandlingTableComponent.prototype, "selectedMachine", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], HandlingTableComponent.prototype, "aliveFlag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */])
    ], HandlingTableComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('table'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["F" /* MatTable */])
    ], HandlingTableComponent.prototype, "table", void 0);
    HandlingTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-handling-table',
            template: __webpack_require__("./src/app/dashboard/handling-table/handling-table.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/handling-table/handling-table.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__handling_data_service__["a" /* HandlingDataService */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__["a" /* HandlingCommService */]])
    ], HandlingTableComponent);
    return HandlingTableComponent;
}());

var JobStatusDataSource = /** @class */ (function (_super) {
    __extends(JobStatusDataSource, _super);
    function JobStatusDataSource(_jobStatusDashboardDatabase, _sort) {
        var _this = _super.call(this) || this;
        _this._jobStatusDashboardDatabase = _jobStatusDashboardDatabase;
        _this._sort = _sort;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    JobStatusDataSource.prototype.connect = function () {
        return this._jobStatusDashboardDatabase.dataChange;
    };
    JobStatusDataSource.prototype.disconnect = function () { };
    JobStatusDataSource.prototype.sortAll = function () {
        this._jobStatusDashboardDatabase.jobStatusData = this.sortData();
    };
    JobStatusDataSource.prototype.sortData = function () {
        var _this = this;
        var data = this._jobStatusDashboardDatabase.jobStatusData;
        if (!this._sort.active || this._sort.direction == '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'u_shift':
                    _a = [a.u_shift, b.u_shift], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'seqno':
                    _b = [a.seqno, b.seqno], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'jobno':
                    _c = [a.jobno, b.jobno], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'state':
                    _d = [a.state, b.state], propertyA = _d[0], propertyB = _d[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction == 'asc' ? 1 : -1);
            var _a, _b, _c, _d;
        });
    };
    return JobStatusDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));

var JobStatusDashboardDatabase = /** @class */ (function () {
    function JobStatusDashboardDatabase(_handlingDataService, _datepipe, _selectedProductionDate, _selectedMachine, _aliveFlag) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._selectedProductionDate = _selectedProductionDate;
        this._selectedMachine = _selectedMachine;
        this._aliveFlag = _aliveFlag;
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.alive = _aliveFlag;
        this.getDataFromService();
    }
    Object.defineProperty(JobStatusDashboardDatabase.prototype, "data", {
        get: function () { return this.jobStatusData; },
        enumerable: true,
        configurable: true
    });
    JobStatusDashboardDatabase.prototype.getDataFromService = function () {
        var _this = this;
        var selectDate = this._datepipe.transform(new Date(), 'yyyyMMdd');
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedProductionDate) || (this._selectedProductionDate == '')) {
            this._selectedProductionDate = selectDate;
        }
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedMachine) || (this._selectedMachine == '')) {
            this._selectedMachine = 'SP';
        }
        this.jobStatusData = [];
        this._handlingDataService.getJobStatusDashboard(this._selectedProductionDate, this._selectedMachine)
            .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(30000)
            .takeWhile(function () { return _this.alive; }) // only fires when component is alive
            .subscribe(function () {
            _this.jobStatusData = [];
            _this._handlingDataService.getJobStatusDashboard(_this._selectedProductionDate, _this._selectedMachine)
                .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        });
    };
    return JobStatusDashboardDatabase;
}());



/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.css":
/***/ (function(module, exports) {

module.exports = ".md-button-right refresh {\r\n    position: absolute;\r\n    right: 0px;\r\n    margin: 0;\r\n    background-color: gold;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-divider></mat-divider>\n<div class='mat-title'>\n    Pending\n</div>\n<div>\n    <mat-divider></mat-divider>\n\n    <mat-form-field>\n      <mat-select placeholder=\"Choose a machine\" [(ngModel)]=\"selectedValue\" >\n        <mat-option *ngFor=\"let mc of machinePendingList\" [value]=\"mc.i_sim_ind_content\">\n          {{ mc.mcp }} ({{ mc.i_sim_ind_content}}) - - - - - --> {{ mc.cnt }}\n        </mat-option>\n      </mat-select>\n    </mat-form-field>\n  \n    <button (click)=\"updateRequest()\" mat-raised-button>View Result</button>\n    \n  \n  </div>\n  <div>\n    <mat-slide-toggle  color=\"primary\" [(ngModel)]=\"refreshToggle\">Auto Refresh!</mat-slide-toggle>\n  </div>\n  <mat-divider></mat-divider>\n  <div>\n    <mat-tab-group>\n      <mat-tab label=\"Pending Status\">\n        <app-jobstatus-pending>\n        </app-jobstatus-pending>\n      </mat-tab>\n    </mat-tab-group>\n  </div>"

/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export AppDateAdapter */
/* unused harmony export APP_DATE_FORMATS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobstatusPendingListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__machine_list_service__ = __webpack_require__("./src/app/dashboard/machine-list.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppDateAdapter = /** @class */ (function (_super) {
    __extends(AppDateAdapter, _super);
    function AppDateAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AppDateAdapter.prototype.parse = function (value) {
        if (typeof value === "string" && value.indexOf("/") > -1) {
            var str = value.split("/");
            var year = Number(str[2]);
            var month = Number(str[1]) - 1;
            var date = Number(str[0]);
            return new Date(year, month, date);
        }
        var timestamp = typeof value === "number" ? value : Date.parse(value);
        return isNaN(timestamp) ? null : new Date(timestamp);
    };
    AppDateAdapter.prototype.format = function (date, displayFormat) {
        if (displayFormat == "input") {
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            //return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
            return year + "" + this._to2digit(month) + "" + this._to2digit(day);
        }
        else {
            return date.toDateString();
        }
    };
    AppDateAdapter.prototype._to2digit = function (n) {
        return ("00" + n).slice(-2);
    };
    return AppDateAdapter;
}(__WEBPACK_IMPORTED_MODULE_2__angular_material__["K" /* NativeDateAdapter */]));

var APP_DATE_FORMATS = {
    parse: {
        dateInput: { month: "short", year: "numeric", day: "numeric" }
    },
    display: {
        // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
        dateInput: "input",
        monthYearLabel: { month: "short", year: "numeric", day: "numeric" },
        dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
        monthYearA11yLabel: { year: "numeric", month: "long" }
    }
};
var JobstatusPendingListComponent = /** @class */ (function () {
    function JobstatusPendingListComponent(_machineListServcie, _handlingComm) {
        this._machineListServcie = _machineListServcie;
        this._handlingComm = _handlingComm;
    }
    JobstatusPendingListComponent.prototype._to2digit = function (n) {
        return ('00' + n).slice(-2);
    };
    JobstatusPendingListComponent.prototype.addEvent = function (type, event) {
        this.productionDate = event.value.getFullYear() + this._to2digit(event.value.getMonth() + 1) + this._to2digit(event.value.getDate());
    };
    JobstatusPendingListComponent.prototype.ngOnInit = function () {
        this.refreshToggle = true;
        this.getMachineList();
        this.getMachinePendingCount();
        console.log(this.machinePendingList);
    };
    JobstatusPendingListComponent.prototype.getMachineList = function () {
        var _this = this;
        this._machineListServcie.getMachineList('')
            .subscribe(function (machine) { return _this.machineList = machine; }, function (error) { return _this.errorMessage = error; });
    };
    JobstatusPendingListComponent.prototype.getMachinePendingCount = function () {
        var _this = this;
        this._machineListServcie.getMachinePendingCount()
            .subscribe(function (mac) { return _this.machinePendingList = mac; }, function (error) { return _this.errorMessage = error; });
    };
    JobstatusPendingListComponent.prototype.updateRequest = function () {
        this._handlingComm.submitData(this.productionDate, this.selectedValue, this.refreshToggle);
    };
    JobstatusPendingListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-jobstatus-pending-list',
            template: __webpack_require__("./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/jobstatus-pending-list/jobstatus-pending-list.component.css")],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["a" /* DateAdapter */], useClass: AppDateAdapter
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MAT_DATE_FORMATS */], useValue: APP_DATE_FORMATS
                }
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__machine_list_service__["a" /* MachineListService */], __WEBPACK_IMPORTED_MODULE_3__handling_comm_service__["a" /* HandlingCommService */]])
    ], JobstatusPendingListComponent);
    return JobstatusPendingListComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.css":
/***/ (function(module, exports) {

module.exports = "/* Structure */\r\n.handling-table-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n.mat-table {\r\n    display: table;\r\n    width: 100%;\r\n    overflow: auto;\r\n    }\r\n.cdk-header-row {\r\n      position: sticky;\r\n      position: -webkit-sticky;\r\n      top: 0;\r\n      background-color: inherit;\r\n  }\r\n/*\r\n   * Styles to make the demo's cdk-table match the material design spec\r\n   * https://material.io/guidelines/components/data-tables.html\r\n   */\r\n.handling-table-table {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n.handling-table-header-row {\r\n    background: #3E50B4 ;\r\n    color: white;\r\n  }\r\n.handling-table-header-row, .handling-table-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border-bottom: 1px solid #ccc;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 32px;\r\n    padding: 0 8px;\r\n  }\r\n.handling-table-cell, .handling-table-header-cell {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1;\r\n            flex: 1;\r\n  }\r\n.handling-table-header-cell {\r\n    font-size: 16px;     \r\n  }\r\n.cdk-cell:nth-child(2),\r\n   .cdk-header-cell:nth-child(2)\r\n    {\r\n    -webkit-box-flex: 0;\r\n        -ms-flex: 0 0 4%;\r\n            flex: 0 0 4%;\r\n    }\r\n.cdk-cell:nth-child(3),\r\n    .cdk-header-cell:nth-child(3)\r\n     {\r\n     -webkit-box-flex: 0;\r\n         -ms-flex: 0 0 4%;\r\n             flex: 0 0 4%;\r\n     }\r\n.cdk-cell:nth-child(4),\r\n     .cdk-header-cell:nth-child(4)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 7%;\r\n              flex: 0 0 7%;\r\n      }\r\n.cdk-cell:nth-child(8),\r\n     .cdk-header-cell:nth-child(8)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 17%;\r\n              flex: 0 0 17%;\r\n      }\r\n.cdk-cell:nth-child(9),\r\n      .cdk-header-cell:nth-child(9)\r\n       {\r\n       -webkit-box-flex: 0;\r\n           -ms-flex: 0 0 17%;\r\n               flex: 0 0 17%;\r\n       }\r\n.handling-table-cell {\r\n    font-size: 13px;\r\n    color: rgba(0, 0, 0, 0.87);\r\n  }\r\n.chip-item {\r\n      width: 100%;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"handling-table-container\">\n    <cdk-table #table  [dataSource]=\"dataSource\" matSortActive=\"u_shift\" matSortDirection='asc' (matSortChange)=\"sortData()\" class=\"handling-table-table\" matSort>\n        <ng-container cdkColumnDef=\"u_plan_date\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_plan_date\">Plan Date</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_plan_date | trim}} </cdk-cell>\n        </ng-container>\n      <ng-container cdkColumnDef=\"u_shift\" >\n                    <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_shift\">Shift</cdk-header-cell>\n                    <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_shift | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"seqno\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" width=\"shrink-to-fit\" mat-sort-header=\"seqno\">Seq</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.seqno}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"jobno\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"jobno\">Job No</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.jobno | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_seiban\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"i_seiban\">Seiban</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_seiban | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_po_detail_no\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"i_po_detail_no\">W/O No.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_po_detail_no | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sch_lot_no\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Cust.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sch_lot_no | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_plan_remark1\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"i_plan_remark1\">Part No.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_plan_remark1 | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_item_cd\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Item Cd</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_item_cd | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_po_qty\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Plan Qty</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_po_qty | number}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"seiban_remark\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Remark</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.seiban_remark | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"state\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"state\">State</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"><mat-chip-list><mat-chip [ngStyle]=\"checkStyle(row.state)\" style='chip-item'>{{row.state}}</mat-chip></mat-chip-list></cdk-cell>\n        </ng-container>\n\n        <cdk-header-row *cdkHeaderRowDef=\"displayedColumns\" class=\"handling-table-header-row\"></cdk-header-row>\n        <cdk-row *cdkRowDef=\"let row; columns: displayedColumns\" class=\"handling-table-row\"></cdk-row>\n\n    </cdk-table>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobstatusPendingComponent; });
/* unused harmony export JobStatusDataSource */
/* unused harmony export JobStatusDashboardDatabase */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_data_service__ = __webpack_require__("./src/app/dashboard/handling-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util__ = __webpack_require__("./node_modules/util/util.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_util___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_util__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var JobstatusPendingComponent = /** @class */ (function () {
    function JobstatusPendingComponent(_handlingDataService, _datepipe, _handlingComm) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._handlingComm = _handlingComm;
        this.displayedColumns = ['u_plan_date', 'u_shift', 'seqno', 'jobno', 'i_seiban',
            'i_po_detail_no', 'i_sch_lot_no', 'i_plan_remark1',
            'i_sim_item_cd', 'i_sim_po_qty', 'seiban_remark', 'state'];
    }
    JobstatusPendingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.active = 'u_shift';
        this.sort.direction = 'asc';
        this._handlingComm.change.subscribe(function (msg) {
            {
                _this.handlingMessage = msg;
                _this.selectedProductionDate = _this.handlingMessage.productionDate;
                _this.selectedMachine = _this.handlingMessage.machine;
                _this.aliveFlag = _this.handlingMessage.alive;
                _this.showData();
            }
        });
    };
    JobstatusPendingComponent.prototype.checkStyle = function (state) {
        var res;
        if (state == 'Finish') {
            res = { 'background-color': '#66BB6A', 'color': 'white' };
        }
        if (state == 'On Process') {
            res = { 'background-color': '#FFCA28' };
        }
        if (state == 'Not Process') {
            res = { 'background-color': '#FFFFFF' };
        }
        return res;
    };
    JobstatusPendingComponent.prototype.showData = function () {
        this.jobStatusDashboardDatabase = new JobStatusDashboardDatabase(this._handlingDataService, this._datepipe, this.selectedProductionDate, this.selectedMachine, this.aliveFlag);
        this.dataSource = new JobStatusDataSource(this.jobStatusDashboardDatabase, this.sort);
    };
    JobstatusPendingComponent.prototype.sortData = function (sort) {
        this.dataSource.sortAll();
        this.table.renderRows();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], JobstatusPendingComponent.prototype, "selectedProductionDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], JobstatusPendingComponent.prototype, "selectedMachine", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], JobstatusPendingComponent.prototype, "aliveFlag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */])
    ], JobstatusPendingComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('table'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["F" /* MatTable */])
    ], JobstatusPendingComponent.prototype, "table", void 0);
    JobstatusPendingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-jobstatus-pending',
            template: __webpack_require__("./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/jobstatus-pending/jobstatus-pending.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__handling_data_service__["a" /* HandlingDataService */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_8__handling_comm_service__["a" /* HandlingCommService */]])
    ], JobstatusPendingComponent);
    return JobstatusPendingComponent;
}());

var JobStatusDataSource = /** @class */ (function (_super) {
    __extends(JobStatusDataSource, _super);
    function JobStatusDataSource(_jobStatusDashboardDatabase, _sort) {
        var _this = _super.call(this) || this;
        _this._jobStatusDashboardDatabase = _jobStatusDashboardDatabase;
        _this._sort = _sort;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    JobStatusDataSource.prototype.connect = function () {
        return this._jobStatusDashboardDatabase.dataChange;
    };
    JobStatusDataSource.prototype.disconnect = function () { };
    JobStatusDataSource.prototype.sortAll = function () {
        this._jobStatusDashboardDatabase.jobStatusData = this.sortData();
    };
    JobStatusDataSource.prototype.sortData = function () {
        var _this = this;
        var data = this._jobStatusDashboardDatabase.jobStatusData;
        if (!this._sort.active || this._sort.direction == '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'u_shift':
                    _a = [a.u_shift, b.u_shift], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'seqno':
                    _b = [a.seqno, b.seqno], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'jobno':
                    _c = [a.jobno, b.jobno], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'state':
                    _d = [a.state, b.state], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'u_plan_date':
                    _e = [a.u_plan_date, b.u_plan_date], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'i_seiban':
                    _f = [a.i_seiban, b.i_seiban], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'i_po_detail_no':
                    _g = [a.i_po_detail_no, b.i_po_detail_no], propertyA = _g[0], propertyB = _g[1];
                    break;
                case 'i_plan_remark1':
                    _h = [a.i_plan_remark1, b.i_plan_remark1], propertyA = _h[0], propertyB = _h[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction == 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g, _h;
        });
    };
    return JobStatusDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));

var JobStatusDashboardDatabase = /** @class */ (function () {
    function JobStatusDashboardDatabase(_handlingDataService, _datepipe, _selectedProductionDate, _selectedMachine, _aliveFlag) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._selectedProductionDate = _selectedProductionDate;
        this._selectedMachine = _selectedMachine;
        this._aliveFlag = _aliveFlag;
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.alive = _aliveFlag;
        this.getDataFromService();
    }
    Object.defineProperty(JobStatusDashboardDatabase.prototype, "data", {
        get: function () { return this.jobStatusData; },
        enumerable: true,
        configurable: true
    });
    JobStatusDashboardDatabase.prototype.getDataFromService = function () {
        var _this = this;
        var selectDate = this._datepipe.transform(new Date(), 'yyyyMMdd');
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedProductionDate) || (this._selectedProductionDate == '')) {
            this._selectedProductionDate = selectDate;
        }
        if (Object(__WEBPACK_IMPORTED_MODULE_7_util__["isNull"])(this._selectedMachine) || (this._selectedMachine == '')) {
            this._selectedMachine = 'SP';
        }
        this.jobStatusData = [];
        this._handlingDataService.getJobPendingDashboard(this._selectedMachine)
            .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(30000)
            .takeWhile(function () { return _this.alive; }) // only fires when component is alive
            .subscribe(function () {
            _this.jobStatusData = [];
            _this._handlingDataService.getJobPendingDashboard(_this._selectedMachine)
                .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        });
    };
    return JobStatusDashboardDatabase;
}());



/***/ }),

/***/ "./src/app/dashboard/machine-list.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MachineListService; });
/* unused harmony export RequestBodyInterface */
/* unused harmony export MachineListModel */
/* unused harmony export MachinePendingCountModel */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MachineListService = /** @class */ (function () {
    function MachineListService(http) {
        this.http = http;
        this.apiUrl = 'http://10.152.18.240:3011/maclist';
    }
    MachineListService.prototype.getMachineList = function (machine) {
        var requestdata = new RequestBodyInterface(machine);
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        //console.log(requestdata);
        //console.log(options);
        return this.http.post(this.apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    MachineListService.prototype.getMachinePendingCount = function () {
        var apiUrl = 'http://10.152.18.240:3011/macpendingcount';
        var requestdata = new RequestBodyInterface('');
        var params = new URLSearchParams();
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
            responseType: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* ResponseContentType */].Json,
            body: requestdata,
            params: params,
            withCredentials: false
        });
        return this.http.post(apiUrl, requestdata, options)
            .map(this.extractData)
            .catch(this.handleError);
    };
    MachineListService.prototype.extractData = function (res) {
        //console.log(res);
        var body = res.json();
        console.log(body);
        return body || {};
    };
    MachineListService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(errMsg);
    };
    MachineListService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], MachineListService);
    return MachineListService;
}());

var RequestBodyInterface = /** @class */ (function () {
    function RequestBodyInterface(_mac) {
        this._mac = _mac;
        this.mcserial = _mac;
    }
    return RequestBodyInterface;
}());

var MachineListModel = /** @class */ (function () {
    function MachineListModel() {
    }
    return MachineListModel;
}());

var MachinePendingCountModel = /** @class */ (function () {
    function MachinePendingCountModel() {
    }
    return MachinePendingCountModel;
}());



/***/ }),

/***/ "./src/app/dashboard/page-not-found/page-not-found.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/page-not-found/page-not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n    Oh! 404 Page not found!\n    Please contact administrator.\n</p>\n  "

/***/ }),

/***/ "./src/app/dashboard/page-not-found/page-not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-page-not-found',
            template: __webpack_require__("./src/app/dashboard/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/page-not-found/page-not-found.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/searchpartno-list/searchpartno-list.component.css":
/***/ (function(module, exports) {

module.exports = ".partno {\r\nmin-width: 150px;\r\nmax-width: 500px;\r\nwidth: 100%;\r\n}"

/***/ }),

/***/ "./src/app/dashboard/searchpartno-list/searchpartno-list.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-divider></mat-divider>\n<div class='mat-title'>\n    Search current by Part No\n</div>\n<div>\n  <mat-divider></mat-divider>\n  <mat-form-field>\n      <input matInput placeholder=\"Customer PartNo\" [(ngModel)]=\"partno\" class=\"partno\">\n  </mat-form-field>\n\n  <button (click)=\"updateRequest()\" mat-raised-button>View Result</button>\n\n</div>\n<div>\n  <mat-slide-toggle  color=\"primary\" [(ngModel)]=\"refreshToggle\">Auto Refresh!</mat-slide-toggle>\n</div>\n<mat-divider></mat-divider>\n<div>\n  <mat-tab-group>\n    <mat-tab label=\"Search Part No\">\n      <app-searchpartno>\n      </app-searchpartno>\n    </mat-tab>\n  </mat-tab-group>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/searchpartno-list/searchpartno-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchpartnoListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchpartnoListComponent = /** @class */ (function () {
    function SearchpartnoListComponent(_handlingComm) {
        this._handlingComm = _handlingComm;
    }
    SearchpartnoListComponent.prototype.ngOnInit = function () {
        this.refreshToggle = true;
    };
    SearchpartnoListComponent.prototype.updateRequest = function () {
        console.log(this.partno);
        this._handlingComm.submitPartno(this.partno);
    };
    SearchpartnoListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-searchpartno-list',
            template: __webpack_require__("./src/app/dashboard/searchpartno-list/searchpartno-list.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/searchpartno-list/searchpartno-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__handling_comm_service__["a" /* HandlingCommService */]])
    ], SearchpartnoListComponent);
    return SearchpartnoListComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/searchpartno/searchpartno.component.css":
/***/ (function(module, exports) {

module.exports = "/* Structure */\r\n.handling-table-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n  }\r\n.mat-table {\r\n    display: table;\r\n    width: 100%;\r\n    overflow: auto;\r\n    }\r\n.cdk-header-row {\r\n      position: sticky;\r\n      position: -webkit-sticky;\r\n      top: 0;\r\n      background-color: inherit;\r\n  }\r\n/*\r\n   * Styles to make the demo's cdk-table match the material design spec\r\n   * https://material.io/guidelines/components/data-tables.html\r\n   */\r\n.handling-table-table {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n    overflow: auto;\r\n    max-height: 500px;\r\n  }\r\n.handling-table-header-row {\r\n    background: #3E50B4 ;\r\n    color: white;\r\n  }\r\n.handling-table-header-row, .handling-table-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border-bottom: 1px solid #ccc;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 32px;\r\n    padding: 0 8px;\r\n  }\r\n.handling-table-cell, .handling-table-header-cell {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1;\r\n            flex: 1;\r\n  }\r\n.handling-table-header-cell {\r\n    font-size: 16px;     \r\n  }\r\n.cdk-cell:nth-child(1),\r\n  .cdk-header-cell:nth-child(1)\r\n   {\r\n   -webkit-box-flex: 0;\r\n       -ms-flex: 0 0 7%;\r\n           flex: 0 0 7%;\r\n   }\r\n.cdk-cell:nth-child(2),\r\n   .cdk-header-cell:nth-child(2)\r\n    {\r\n    -webkit-box-flex: 0;\r\n        -ms-flex: 0 0 4%;\r\n            flex: 0 0 4%;\r\n    }\r\n.cdk-cell:nth-child(3),\r\n    .cdk-header-cell:nth-child(3)\r\n     {\r\n     -webkit-box-flex: 0;\r\n         -ms-flex: 0 0 3%;\r\n             flex: 0 0 3%;\r\n     }\r\n.cdk-cell:nth-child(4),\r\n     .cdk-header-cell:nth-child(4)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 8%;\r\n              flex: 0 0 8%;\r\n      }\r\n.cdk-cell:nth-child(7),\r\n     .cdk-header-cell:nth-child(7)\r\n      {\r\n      -webkit-box-flex: 0;\r\n          -ms-flex: 0 0 15%;\r\n              flex: 0 0 15%;\r\n      }\r\n.cdk-cell:nth-child(8),\r\n      .cdk-header-cell:nth-child(8)\r\n       {\r\n       -webkit-box-flex: 0;\r\n           -ms-flex: 0 0 15%;\r\n               flex: 0 0 15%;\r\n       }\r\n.handling-table-cell {\r\n    font-size: 13px;\r\n    color: rgba(0, 0, 0, 0.87);\r\n  }\r\n.chip-item {\r\n      width: 100%;\r\n  }"

/***/ }),

/***/ "./src/app/dashboard/searchpartno/searchpartno.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"handling-table-container\">\n    <cdk-table #table  [dataSource]=\"dataSource\" matSortActive=\"u_shift\" matSortDirection='asc' (matSortChange)=\"sortData()\" class=\"handling-table-table\" matSort>\n        <ng-container cdkColumnDef=\"u_plan_date\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_plan_date\">Plan Date</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_plan_date | trim}} </cdk-cell>\n        </ng-container>\n      <ng-container cdkColumnDef=\"u_shift\" >\n                    <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_shift\">Shift</cdk-header-cell>\n                    <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_shift | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"seqno\" >\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" width=\"shrink-to-fit\" mat-sort-header=\"seqno\">Seq</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.seqno}} </cdk-cell>\n        </ng-container>\n\n        <ng-container cdkColumnDef=\"u_ind_content\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"u_ind_content\">Plan Process</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.u_ind_content | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_seiban\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"i_seiban\">Seiban</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_seiban | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sch_lot_no\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Cust.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sch_lot_no | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_plan_remark1\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Part No.</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_plan_remark1 | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_item_cd\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Item Cd</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_item_cd | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_sim_po_qty\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Plan Qty</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_sim_po_qty | number}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"i_stock_location\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Next Process</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.i_stock_location | trim}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_start\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Start</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_start | date:\"HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"prc_end\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\">Finish</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"> {{row.prc_end | date:\"HH:mm\"}} </cdk-cell>\n        </ng-container>\n        <ng-container cdkColumnDef=\"state\">\n            <cdk-header-cell *cdkHeaderCellDef class=\"handling-table-header-cell\" mat-sort-header=\"state\">State</cdk-header-cell>\n            <cdk-cell *cdkCellDef=\"let row\" class=\"handling-table-cell\"><mat-chip-list><mat-chip [ngStyle]=\"checkStyle(row.state)\" style='chip-item'>{{row.state}}</mat-chip></mat-chip-list></cdk-cell>\n        </ng-container>\n\n        <cdk-header-row *cdkHeaderRowDef=\"displayedColumns\" class=\"handling-table-header-row\"></cdk-header-row>\n        <cdk-row *cdkRowDef=\"let row; columns: displayedColumns\" class=\"handling-table-row\"></cdk-row>\n\n    </cdk-table>\n\n  </div>"

/***/ }),

/***/ "./src/app/dashboard/searchpartno/searchpartno.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchpartnoComponent; });
/* unused harmony export JobStatusDataSource */
/* unused harmony export JobStatusDashboardDatabase */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__handling_data_service__ = __webpack_require__("./src/app/dashboard/handling-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__handling_comm_service__ = __webpack_require__("./src/app/dashboard/handling-comm.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/_esm5/Rx.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var SearchpartnoComponent = /** @class */ (function () {
    function SearchpartnoComponent(_handlingDataService, _datepipe, _handlingComm) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._handlingComm = _handlingComm;
        this.displayedColumns = ['u_plan_date', 'u_shift', 'seqno', 'u_ind_content', 'i_seiban', 'i_sch_lot_no',
            'i_plan_remark1',
            'i_sim_item_cd', 'i_sim_po_qty', 'i_stock_location',
            'prc_start', 'prc_end', 'state'];
    }
    SearchpartnoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sort.active = 'u_shift';
        this.sort.direction = 'asc';
        this._handlingComm.change.subscribe(function (msg) {
            {
                _this.handlingMessage = msg;
                _this.partno = _this.handlingMessage.partno;
                _this.showData();
            }
        });
    };
    SearchpartnoComponent.prototype.checkStyle = function (state) {
        var res;
        if (state == 'Finish') {
            res = { 'background-color': '#66BB6A', 'color': 'white' };
        }
        if (state == 'On Process') {
            res = { 'background-color': '#FFCA28' };
        }
        if (state == 'Not Process') {
            res = { 'background-color': '#FFFFFF' };
        }
        return res;
    };
    SearchpartnoComponent.prototype.showData = function () {
        this.jobStatusDashboardDatabase = new JobStatusDashboardDatabase(this._handlingDataService, this._datepipe, this.partno, this.aliveFlag);
        this.dataSource = new JobStatusDataSource(this.jobStatusDashboardDatabase, this.sort);
    };
    SearchpartnoComponent.prototype.sortData = function (sort) {
        this.dataSource.sortAll();
        this.table.renderRows();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", String)
    ], SearchpartnoComponent.prototype, "partno", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
        __metadata("design:type", Boolean)
    ], SearchpartnoComponent.prototype, "aliveFlag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["C" /* MatSort */])
    ], SearchpartnoComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('table'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["F" /* MatTable */])
    ], SearchpartnoComponent.prototype, "table", void 0);
    SearchpartnoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-searchpartno',
            template: __webpack_require__("./src/app/dashboard/searchpartno/searchpartno.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/searchpartno/searchpartno.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__handling_data_service__["a" /* HandlingDataService */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */],
            __WEBPACK_IMPORTED_MODULE_7__handling_comm_service__["a" /* HandlingCommService */]])
    ], SearchpartnoComponent);
    return SearchpartnoComponent;
}());

var JobStatusDataSource = /** @class */ (function (_super) {
    __extends(JobStatusDataSource, _super);
    function JobStatusDataSource(_jobStatusDashboardDatabase, _sort) {
        var _this = _super.call(this) || this;
        _this._jobStatusDashboardDatabase = _jobStatusDashboardDatabase;
        _this._sort = _sort;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    JobStatusDataSource.prototype.connect = function () {
        return this._jobStatusDashboardDatabase.dataChange;
    };
    JobStatusDataSource.prototype.disconnect = function () { };
    JobStatusDataSource.prototype.sortAll = function () {
        this._jobStatusDashboardDatabase.jobStatusData = this.sortData();
    };
    JobStatusDataSource.prototype.sortData = function () {
        var _this = this;
        var data = this._jobStatusDashboardDatabase.jobStatusData;
        if (!this._sort.active || this._sort.direction == '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'u_shift':
                    _a = [a.u_shift, b.u_shift], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'seqno':
                    _b = [a.seqno, b.seqno], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'jobno':
                    _c = [a.jobno, b.jobno], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'state':
                    _d = [a.state, b.state], propertyA = _d[0], propertyB = _d[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction == 'asc' ? 1 : -1);
            var _a, _b, _c, _d;
        });
    };
    return JobStatusDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));

var JobStatusDashboardDatabase = /** @class */ (function () {
    function JobStatusDashboardDatabase(_handlingDataService, _datepipe, _partno, _aliveFlag) {
        this._handlingDataService = _handlingDataService;
        this._datepipe = _datepipe;
        this._partno = _partno;
        this._aliveFlag = _aliveFlag;
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */]([]);
        this.alive = _aliveFlag;
        this.getDataFromService();
        this._seiban_list = this.callseiban_list(this.jobStatusData);
    }
    Object.defineProperty(JobStatusDashboardDatabase.prototype, "data", {
        get: function () { return this.jobStatusData; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(JobStatusDashboardDatabase.prototype, "seiban_list", {
        get: function () { return this._seiban_list; },
        enumerable: true,
        configurable: true
    });
    JobStatusDashboardDatabase.prototype.callseiban_list = function (jobstatus) {
        var source;
        var tmp;
        for (var _i = 0, jobstatus_1 = jobstatus; _i < jobstatus_1.length; _i++) {
            var job = jobstatus_1[_i];
            if (tmp != job.i_seiban) {
                source.push(job.i_seiban);
                tmp = job.i_seiban;
            }
        }
        return source;
    };
    JobStatusDashboardDatabase.prototype.getDataFromService = function () {
        var _this = this;
        var selectDate = this._datepipe.transform(new Date(), 'yyyyMMdd');
        this.jobStatusData = [];
        this._handlingDataService.getJobResultByPartnoDashboard(this._partno)
            .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(30000)
            .takeWhile(function () { return _this.alive; }) // only fires when component is alive
            .subscribe(function () {
            _this.jobStatusData = [];
            _this._handlingDataService.getJobResultByPartnoDashboard(_this._partno)
                .subscribe(function (datajob) { _this.jobStatusData = datajob; _this.dataChange.next(_this.jobStatusData); }, function (error) { return _this.errorMessage = error; });
        });
    };
    return JobStatusDashboardDatabase;
}());



/***/ }),

/***/ "./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  status-current-all-machines works!\n</p>\n"

/***/ }),

/***/ "./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusCurrentAllMachinesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StatusCurrentAllMachinesComponent = /** @class */ (function () {
    function StatusCurrentAllMachinesComponent() {
    }
    StatusCurrentAllMachinesComponent.prototype.ngOnInit = function () {
    };
    StatusCurrentAllMachinesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-status-current-all-machines',
            template: __webpack_require__("./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/status-current-all-machines/status-current-all-machines.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], StatusCurrentAllMachinesComponent);
    return StatusCurrentAllMachinesComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/helper/pipe/filterUnique.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterUniquePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterUniquePipe = /** @class */ (function () {
    function FilterUniquePipe() {
    }
    FilterUniquePipe.prototype.transform = function (value, args) {
        // Remove the duplicate elements
        var uniqueArray = value.filter(function (el, index, array) {
            return array.indexOf(el) == index;
        });
        return uniqueArray;
    };
    FilterUniquePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Pipe */])({
            name: 'filterUnique',
            pure: false
        })
    ], FilterUniquePipe);
    return FilterUniquePipe;
}());



/***/ }),

/***/ "./src/helper/pipe/trim.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrimPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TrimPipe = /** @class */ (function () {
    function TrimPipe() {
    }
    TrimPipe.prototype.transform = function (value) {
        if (!value) {
            return '';
        }
        return value.trim();
    };
    TrimPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Pipe */])({ name: 'trim' })
    ], TrimPipe);
    return TrimPipe;
}());



/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map