import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { JobStatusDashboardModel } from './shared/jobstatusdashboard.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class HandlingDataService {

    constructor (private http: Http) {}
    getJobStatusDashboard (plandate: string, machine: string): Observable<JobStatusDashboardModel[]> {
      const apiUrl = 'http://10.152.18.240:3011/jobstatus';
      const requestdata = new RequestBodyInterface(plandate, machine);
      const params = new URLSearchParams();


      const options = new RequestOptions({
      responseType: ResponseContentType.Json,
      body: requestdata,
      params: params,
      withCredentials: false
    });

      return this.http.post(apiUrl,  requestdata, options)
                      .map(this.extractData)
                      .catch(this.handleError);
    }

    getJobResultDashboard (plandate: string, machine: string): Observable<JobStatusDashboardModel[]> {
      const apiUrl = 'http://10.152.18.240:3011/jobresult';
      const requestdata = new RequestBodyInterface(plandate, machine);
      const params = new URLSearchParams();


      const options = new RequestOptions({
      responseType: ResponseContentType.Json,
      body: requestdata,
      params: params,
      withCredentials: false
    });

      return this.http.post(apiUrl,  requestdata, options)
                      .map(this.extractData)
                      .catch(this.handleError);
    }



    getJobResultByPartnoDashboard (partno: string): Observable<JobStatusDashboardModel[]> {
      const apiUrl = 'http://10.152.18.240:3011/searchresultbypartno';
      let requestdata = new RequestBodyInterface('', '');
      requestdata.setPartno(partno);

      const params = new URLSearchParams();
      const options = new RequestOptions({
      responseType: ResponseContentType.Json,
      body: requestdata,
      params: params,
      withCredentials: false
    });

      return this.http.post(apiUrl,  requestdata, options)
                      .map(this.extractData)
                      .catch(this.handleError);
    }


    getJobPendingDashboard (machine: string): Observable<JobStatusDashboardModel[]> {
      const apiUrl = 'http://10.152.18.240:3011/jobpending';
      const requestdata = new RequestBodyInterface('', machine);
      const params = new URLSearchParams();


      const options = new RequestOptions({
      responseType: ResponseContentType.Json,
      body: requestdata,
      params: params,
      withCredentials: false
    });

      return this.http.post(apiUrl,  requestdata, options)
                      .map(this.extractData)
                      .catch(this.handleError);
    }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }
    private handleError (error: any) {
     let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      return Observable.throw(errMsg);
    }
  }

export class RequestBodyInterface {
  private dts: string;
  private mac: string;
  private partno: string;
  constructor(private _dts: string, private _mac: string) {
      this.dts = _dts;
      this.mac = _mac;
  }

  setPartno(partno: string) {
    this.partno = partno;
  }
}

