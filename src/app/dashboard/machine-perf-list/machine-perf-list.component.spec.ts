import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinePerfListComponent } from './machine-perf-list.component';

describe('MachinePerfListComponent', () => {
  let component: MachinePerfListComponent;
  let fixture: ComponentFixture<MachinePerfListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinePerfListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinePerfListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
