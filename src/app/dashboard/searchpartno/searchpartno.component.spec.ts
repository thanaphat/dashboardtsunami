import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchpartnoComponent } from './searchpartno.component';

describe('SearchpartnoComponent', () => {
  let component: SearchpartnoComponent;
  let fixture: ComponentFixture<SearchpartnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchpartnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchpartnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
