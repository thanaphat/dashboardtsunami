import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class MachineCommService {

	constructor() { }
	
	_machineComm: MachineCoummunicationInterface = {
		productionDate: '',
		shift: ''
	}
	
	@Output('')
	change: EventEmitter<MachineCoummunicationInterface> = new EventEmitter();
	
	submitData(_productionDate: string, _shift: string){
		this._machineComm.productionDate = _productionDate;
		this._machineComm.shift = _shift;
		
		this.change.emit(this._machineComm);
	}
	
}

export class MachineCoummunicationInterface {
	productionDate: string;
    shift: string;
}