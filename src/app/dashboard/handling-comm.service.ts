import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable()
export class HandlingCommService {

  constructor() { }

  _handlingComm: HandlingCommunicationInterface = {
      productionDate: '',
      machine: '',
      partno: '',
      alive: false
  };

  @Output()
  change: EventEmitter<HandlingCommunicationInterface> = new EventEmitter();

  submitData(_productionDate: string, _machine: string, _alive: boolean) {
    this._handlingComm.productionDate = _productionDate;
    this._handlingComm.machine = _machine;
    this._handlingComm.alive = _alive;
    this.change.emit(this._handlingComm);
  }

  submitPartno(_partno: string) {
    this._handlingComm.partno = _partno;
    this.change.emit(this._handlingComm);
  }


}


export class HandlingCommunicationInterface {
    productionDate: string;
    machine: string;
    partno: string;
    alive: boolean;
}