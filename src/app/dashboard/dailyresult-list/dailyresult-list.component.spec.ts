import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyresultListComponent } from './dailyresult-list.component';

describe('DailyresultListComponent', () => {
  let component: DailyresultListComponent;
  let fixture: ComponentFixture<DailyresultListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyresultListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyresultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
